��            )   �      �     �     �     �     �  @   �  _   !     �     �  b   �       F   ,  w   s     �     �            I   0     z  �   �  ?   @     �  0   �     �  �   �  Y   ]  
   �     �     �  �   �  K  �     �	     �	     
  
   
  D   
  P   d
     �
     �
  M   �
     :     Y  Z   v     �     �     �       J   *     u  �   ~  :   4     o  &   |  
   �  r   �  H   !     j     q     �  ?   �                                                  
                                                       	                                /Cesium-G1-maquette.png /accueil /developpeurs /telechargement Cesium : la solution simple<br />pour échanger en monnaie libre Cesium est l'application la plus intuitive pour gérer votre portefeuille en monnaie-libre Ğ1. Cesium est un logiciel libre. Cesium partout Cesium signe numériquement vos transactions avant de les transmettre à la blockchain de Duniter. Cesium Ğ1 - Site officiel Choisissez un identifiant secret et un mot de passe tout aussi secret. Cliquez sur votre clef publique, copiez-la et transmettez-la à la personne qui souhaite vous faire un virement en Ğ1. Créez un compte Développeur ? Libre Logiciel libre sous licence %s Mémorisez-les bien car il ne sera pas possible de les retrouver ensuite. Offert Parce qu'il est codé avec amour par une communauté de gens qui croient aux monnaies libres en général (et à la Ğ1 en particulier), Cesium vous est offert sans exiger de contre-partie.  Plus que quelques pas<br />avant de recevoir vos premières Ğ1 Recevez des paiements Recevez et envoyez <br />de la monnaie libre Ğ1 Rejoignez-nous ! Rendez-vous sur <a href="%s">la page Téléchargement</a> pour récupérer le fichier qui convient à votre système d'exploitation. Sur l'accueil, cliquez sur "Créer un compte", puis "Commencer" et "Simple portefeuille". Sécurisé Télécharger Cesium %s Téléchargez Cesium Vous êtes donc libre d'en consulter le code source (sur <a href="%s">GitLab</a>), et d'adapter Cesium à vos besoins pour, par exemple, lancer votre propre monnaie libre. Project-Id-Version: 
PO-Revision-Date: 2019-11-07 18:39+0100
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
X-Poedit-Basepath: ../../../tpl
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: home.php
 /Cesium-G1-modelo.png /inicio /desarrollador /descargar Cesium : la solución sencilla<br />para cambiar con la moneda libre Cesium es la aplicación más intuitiva para cambiar usando la moneda libre Ğ1. Cesium está un software libre. Cesium en todas partes Cesium firma tus transacciones antes de transmitirles a la blockchain Duniter Cesium Ğ1 - Sitio web oficial Elige tu login y tu código. Clica sobre tu clave publica para copiarla. Envíala a la persona que quiere enviarte Ğ1. Crea tu cuenta Desarrollador de software ? Software libre Software libre con licencia %s Memorizales bien porche, si les pierdes después, no podría encontrarlos. Ofrecido Cesium está codificada con amor por una comunidad de personas que creen en las monedas libres en general (y en la Ğ1 en particular). Se le ofrece Cesium sin exgir una contraparte. Sólo algunos clics<br />antes de recibir sus primeras Ğ1 Recibe pagos Recibir y enviar<br />moneda libre Ğ1 Ayúdanos! Visita <a href="%s">la página de Descarga</a> para obtener el fichero que está apropiado a tu sistema operativo. Haz un clic sobre "Create an account", y "Start" y elige"Simple wallet". Siguro Descargar Cesium %s Descargar Cesium Puedes ver a su código fuente (sobre <a href="%s">GitLab</a>). 